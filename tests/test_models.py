import unittest
from sellorder_api import create_app, db
from sellorder_api.models import EnergyPlans


class ModelTest(unittest.TestCase):
    """docstring for ModelTest"""

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        p1 = EnergyPlans(
            plan_name='Gold',
            daily_rate=100.0,
            supply_duration=10,
            instantaneous_load_limit=200,
            daily_energy_limit=2000
        )
        db.session.add(p1)
        db.session.commit()
        db.session.close()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_create_plan(self):
        p1 = EnergyPlans(
            plan_name='Gold',
            daily_rate=100.0,
            supply_duration=10,
            instantaneous_load_limit=200,
            daily_energy_limit=2000
        )
        db.session.add(p1)
        db.session.commit()
        db.session.close()
        self.assertTrue(p1.plan_name == 'Gold')
        self.assertTrue(p1.daily_rate == 100.0)
        self.assertTrue(p1.supply_duration == 10)
        self.assertTrue(p1.instantaneous_load_limit == 200)
        self.assertTrue(p1.daily_energy_limit == 2000)
        self.assertFalse(p1.plan_name == 'Silver')
        self.assertFalse(p1.daily_rate == 1000.0)
        self.assertFalse(p1.supply_duration == 11)
        self.assertFalse(p1.instantaneous_load_limit == 205)
        self.assertFalse(p1.daily_energy_limit == 2300)

    def test_get_all_plans(self):
        pass


if __name__ == "__main__":
    unittest.main()
