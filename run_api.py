# Test Server.
from api import create_api, db
from os import environ
from flask_migrate import Migrate, upgrade
from flask import request
from arrow import now
from logger import logger

if environ.get('FLASK_ENV') is None:
    print('FLASK_ENV not set')
app = create_api(environ.get('FLASK_ENV') or 'development')

migrate = Migrate(app, db)
to_migrate = app.config.get('MIGRATE_DB')

@app.after_request
def log_info(response):
    try:
        log_data = {
            'connection': request.headers.get('Connection'),
            'ip_address': request.remote_addr,
            'browser_name': request.user_agent.browser,
            'user_device': request.user_agent.platform,
            'referrer': request.referrer,
            'request_url': request.url,
            'host_url': request.host_url,
            'status_code': response.status_code,
            'date': str(now('Africa/Lagos')),
            'location': response.location,
        }
        logger.info('sellorder_api_logs: {}'.format(log_data))

    except Exception as e:
        logger.exception("sellorder_api_logs: {}".format(e))
        pass
    return response


with app.app_context():
    db.create_all()

if to_migrate is True:
    with app.app_context():
        try:
            upgrade()
        except Exception as e:
            logger.warn(e)
            db.session.rollback()
