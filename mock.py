from requests import get, post, put, delete


payload = {
    'plan_name': 'Gold',
    'daily_rate': 123,
    'supply_duration': 10,
    'instantaneous_load_limit': 200,
    'daily_energy_limit': 2000,
}


# r = get('http://localhost:5100/sell/all/')
r = post('http://localhost:5100/sell/new-plan', data=payload)
# r = delete('http://localhost:5100/sell/Gold')
# r = put('http://localhost:5100/sell/gold', data=payload)
print(r.json())
print(r.status_code)
