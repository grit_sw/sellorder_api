import arrow

from api.models import SellOrder
from logger import logger


class OrderManager(object):
	"""docstring for OrderManager"""

	def create_order(self, payload, admin_id=None):
		"""
			Method to create a sell order
			@args: payload
			@returns: status code
		"""
		response = {}
		facility_id = payload['facility_id']
		subnet_id = payload['subnet_id']
		user_group_id = payload['user_group_id']
		modified_payload = payload.copy()
		start_date_time = arrow.get(payload['start_date_time']).replace(tzinfo="Africa/Lagos")
		end_date_time = arrow.get(payload['end_date_time']).replace(tzinfo="Africa/Lagos")
		modified_payload['start_date_time'] = str(start_date_time)
		modified_payload['end_date_time'] = str(end_date_time)

		clashing_times = SellOrder.clashing_times(user_group_id, subnet_id, facility_id, start_date_time, end_date_time)
		clash_details = [order.to_dict() for order in clashing_times]
		message = 'An order with a conflicting time was found. You might want to review this order.'
		if len(clashing_times) > 1:
			logger.info("\n\n\n\n")
			logger.info("CLASHING TIMES = {}".format(clash_details))
			logger.info("\n\n\n\n")
			message = 'Conflicting order times were found. You might want to review these orders.'

		if clashing_times:
			response['success'] = False
			response['message'] = message
			response['data'] = clash_details
			return response, 409

		orders = SellOrder.all_my_orders(user_group_id=user_group_id).all()
		duplicate = False
		dupe_order = None
		for order in orders:
			if order.payload_format() == modified_payload:
				duplicate = True
				dupe_order = order
				break

		if duplicate:
			response['success'] = False
			response['message'] = 'Order already exists.'
			response['data'] = dupe_order.to_dict()
			return response, 409

		if admin_id:
			payload['created_by'] = admin_id
		else:
			payload['created_by'] = payload['user_group_id']

		data, success = SellOrder.create_order(**payload)
		if not success:
			logger.info(payload)
			response['success'] = success
			response['message'] = str(data)
			return response, 400

		response['success'] = success
		response['data'] = data
		return response, 201

	def all_orders_in_facility(self, facility_id, page_no, size):
		"""
			An admin function to view all the orders in a market
		"""
		response = {}
		orders = SellOrder.facility_orders(facility_id, page_no, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No sell orders found.'
			return response, 404

		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def all_subnet_orders(self, subnet_id, page_no, size):
		"""
			An admin function to view all the orders in a market
		"""
		response = {}
		orders = SellOrder.subnet_orders(subnet_id, page_no, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No sell orders found.'
			return response, 404

		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def billing_subnet_orders(self, subnet_id, billing_unit_id, page_no, size):
		response = {}
		orders = SellOrder.billing_subnet_orders(subnet_id, billing_unit_id, page_no, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No sell orders found.'
			return response, 404

		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def one_order(self, order_id):
		response = {}
		order = SellOrder.one_order(order_id)

		if not order:
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 404

		response['success'] = True
		response['data'] = order
		return response, 200

	def my_orders(self, user_group_id, page, size):
		response = {}
		orders = SellOrder.my_orders(user_group_id, page, size)
		all_orders = [order.to_dict() for order in orders.items]

		if not all_orders:
			response['success'] = False
			response['message'] = 'No sell orders found.'
			return response, 404
		response['success'] = True
		response['data'] = all_orders
		response['count'] = len(all_orders)
		response['has_prev'] = orders.has_prev
		response['has_next'] = orders.has_next
		return response, 200

	def edit_order(self, payload):
		response = {}
		payload = dict(payload)
		order_id = payload['order_id']
		order = SellOrder.get_order(order_id)
		if order is None:
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 404

		modified_payload = payload.copy()
		start_date_time = arrow.get(payload['start_date_time']).replace(tzinfo="Africa/Lagos")
		end_date_time = arrow.get(payload['end_date_time']).replace(tzinfo="Africa/Lagos")
		modified_payload['start_date_time'] = str(start_date_time)
		modified_payload['end_date_time'] = str(end_date_time)

		del modified_payload['order_id']
		del modified_payload['facility_id']
		del modified_payload['user_group_id']

		if dict(modified_payload) == order.edit_format():
			response['success'] = False
			response['message'] = 'No changes detected.'
			return response, 409

		user_group_id = payload['user_group_id']
		if user_group_id != order.user_group_id:
			response['success'] = False
			response['message'] = 'User mismatch.'
			return response, 403

		facility_id = payload['facility_id']
		if facility_id != order.facility_id:
			response['success'] = False
			response['message'] = 'Facility mismatch.'
			return response, 403

		data = SellOrder.edit_order(order, payload)
		response['success'] = True
		response['data'] = data
		return response, 200

	def find_order_match(self, payload):
		"""Get all the sell orders that match a given buy order"""
		response = {}

		del payload['auto_renew']
		del payload['subnet_id']
		del payload['user_group_id']
		matches = SellOrder.find_order_match(**payload)
		if matches:
			response['success'] = True
			response['data'] = [match.to_dict() for match in matches]
			return response, 200

		response['success'] = False
		response['message'] = 'No match found.'
		return response, 404

	def delete_order(self, order_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order = SellOrder.get_order(order_id=order_id)
		if order is None:
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 404

		SellOrder.delete_order(order)
		response['success'] = True
		response['message'] = 'Sell Order deleted.'
		return '', 204

	def delete_user_group(self, user_group_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order_obj = SellOrder.get_user_order(user_group_id=user_group_id)
		if order_obj is None:
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 404
		try:
			SellOrder.delete_all_orders(order_obj)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 500

		response['success'] = True
		response['message'] = 'Sell Order deleted.'
		return '', 204

	def delete_facility(self, facility_id):
		# Ensure only a customer or a client-admin can execute this
		response = {}
		order_obj = SellOrder.get_facility_order(facility_id=facility_id)
		if order_obj is None:
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 404
		try:
			SellOrder.delete_all_orders(order_obj)
		except Exception as e:
			logger.critical(e)
			response['success'] = False
			response['message'] = 'Sell order not found.'
			return response, 500

		response['success'] = True
		response['message'] = 'Sell Order deleted.'
		return '', 204
