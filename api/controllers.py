from flask import request
from flask_restplus import Namespace, Resource, fields
from marshmallow import ValidationError

from api import api
from api.manager import OrderManager
from api.schema import SellOrderSchema
from logger import logger

sell_api = Namespace('sell', description='Api for managing sell orders')
order = sell_api.model('Sell Order', {
	'billing_unit_id': fields.String(required=True, description="Billing unit id of user"),
	'rate': fields.Integer(required=True, description='Sell order rate a user sets'),
	'power_limit': fields.Float(required=False, description="Power limit of user"),
	'energy_limit': fields.Float(required=False, description="Energy limit of user"),
	'auto_renew': fields.Boolean(required=True, description='Indicates if the user wants automatically renew their sell order'),
	'renew_frequency': fields.String(required=True, description='Shows how often the user wants to renew their sell order'),
	'start_date_time': fields.String(description='Start time for daily schedule of sell order'),
	'end_date_time': fields.String(description='Stop time for daily schedule of sell order'),
	'user_group_id': fields.String(required=True, description='User group ID of the user.'),
	'facility_id': fields.String(required=True, description='Facility ID of the user.'),
	'subnet_id': fields.String(required=True, description='Subnet ID of the user.'),
	'prosumer_id': fields.String(required=True, description='Prosumer ID of the user.'),
})


@sell_api.route('/self-new-order/')
class NewSelfSellOrder(Resource):
	"""
		Api to create a new energy order
	"""
	@sell_api.expect(order)
	def post(self):
		"""
			Method for a user to create a new sell order for herself.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = SellOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		group_id = request.cookies.get('group_id')

		# role_id = 5
		# group_id = "String"
		try:
			if not all([role_id, group_id]) or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403
		payload['user_group_id'] = group_id
		manager = OrderManager()
		resp, code = manager.create_order(payload)
		return resp, code


@sell_api.route('/admin-new-order/')
class NewAdminSellOrder(Resource):
	"""
		Api to create a new energy order
	"""
	@sell_api.expect(order)
	def post(self):
		"""
			Method for an admin to create a new sell order for a user.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = SellOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		admin_id = request.cookies.get('user_id')
		# role_id = 5
		# admin_id = "string"
		try:
			if not all([role_id, admin_id]) or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.create_order(payload, admin_id=admin_id)
		return resp, code


@sell_api.route('/all/<string:facility_id>/')
class AllSellOrders(Resource):
	"""
		Api to manage all sell orders
	"""

	def get(self, facility_id):
		"""
			HTTP method to get all sell orders
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		facilities = request.cookies.get('facilities')
		# facilities = {facility_id : "b8fd6b43-90d7-4bce-ac06-33f5c22e2728", facility_id : "string"}

		if not facility_id in set(facilities):
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)

		resp, code = manager.all_orders_in_facility(facility_id, page, size)
		return resp, code


@sell_api.route('/by-subnet/<string:subnet_id>/')
@sell_api.doc(params={
	'subnet_id': 'The subnet ID', 
	'page': 'The page number you want to view',
	'size': 'The number of items you want to retrieve from the database with each request.'
	}
)
class SubnetSellOrders(Resource):
	"""
		Api to manage all sell orders of a subnet.
	"""

	def get(self, subnet_id):
		"""
			GET all sell orders of a subnet.
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)

		resp, code = manager.all_subnet_orders(subnet_id, page, size)
		return resp, code


@sell_api.route('/by-billing-subnet/<string:subnet_id>/<string:billing_unit_id>/')
class SubnetandBillingUnitSellOrders(Resource):
	"""
		Api to manage all sell orders of a subnet.
	"""

	def get(self, subnet_id, billing_unit_id):
		"""
			GET all sell orders of a subnet.
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)

		resp, code = manager.billing_subnet_orders(subnet_id, billing_unit_id, page, size)
		return resp, code


@sell_api.route('/one-order/<string:order_id>/')
class OneSellOrder(Resource):
	"""
		Api to manage individual sell orders
	"""
	def get(self, order_id):
		"""
			HTTP method to get a customer's sell order
			@param: customer_id: ID of the customer
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if role_id is None or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.one_order(order_id)
		return resp, code


@sell_api.route('/my-orders/')
class MySellOrders(Resource):
	"""
		Api to manage individual sell orders
	"""
	def get(self):
		"""
			Route to view all the sell orders a user has.
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		group_id = request.cookies.get('group_id')
		# role_id = 5
		# group_id = "String"
		try:
			if not all([group_id, role_id]) or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		page = request.args.get('page', 1, type=int)
		size = request.args.get('size', 20, type=int)
		manager = OrderManager()
		resp, code = manager.my_orders(group_id, page, size)
		return resp, code


@sell_api.route('/edit-order/<string:order_id>/')
class EditSellOrder(Resource):
	"""
		Api to create a edit energy order
	"""
	@sell_api.expect(order)
	def post(self, order_id):
		"""
			Method for a user to create a edit sell order for herself.
			@param: payload: data for the update
			@returns: response and status code
		"""
		response = {}
		schema = SellOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if not role_id or int(role_id) < 1:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403
		manager = OrderManager()
		payload['order_id'] = order_id
		resp, code = manager.edit_order(payload)
		return resp, code


@sell_api.route('/match-buyorder/')
class FindSellOrderMatch(Resource):

	@sell_api.expect(order)
	def post(self):
		"""
			Get all sell orders that match a buy order
			@returns: response and status code
		"""
		response = {}
		schema = SellOrderSchema(strict=True)
		data = request.values.to_dict()
		_payload = api.payload or data
		try:
			payload = schema.load(_payload).data._asdict()
		except ValidationError as e:
			logger.exception(e.messages)
			response = {}
			response['success'] = False
			response['message'] = e.messages
			return response, 400

		role_id = request.cookies.get('role_id')
		try:
			if role_id is None or int(role_id) < 6:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.find_order_match(payload)
		return resp, code


@sell_api.route('/delete-order/<string:order_id>/')
class DeleteSellOrder(Resource):
	def delete(self, order_id):
		"""
			HTTP method to delete a sell order
			@param: order_id: ID of the order
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if role_id is None or int(role_id) < 2:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.delete_order(order_id)
		return resp, code


@sell_api.route('/delete-user-group/<string:user_group_id>/')
class DeleteUserGroupSellOrders(Resource):
	def delete(self, user_group_id):
		"""
			HTTP method to delete all the sell orders in a user group
			@param: user_group_id: ID of the user group
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if role_id is None or int(role_id) < 3:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.delete_user_group(user_group_id)
		return resp, code


@sell_api.route('/delete-facility/<string:facility_id>/')
class DeleteFacilitySellOrders(Resource):
	def delete(self, facility_id):
		"""
			HTTP method to delete all the sell orders in a facility
			@param: facility_id: ID of the user group
			@returns: response and status code
		"""
		response = {}
		role_id = request.cookies.get('role_id')
		# role_id = 5
		try:
			if role_id is None or int(role_id) < 5:
				response['success'] = False
				response['message'] = "Unauthourized"
				return response, 403
		except ValueError as e:
			logger.exception(e)
			response['success'] = False
			response['message'] = "Unauthourized"
			return response, 403

		manager = OrderManager()
		resp, code = manager.delete_facility(facility_id)
		return resp, code
