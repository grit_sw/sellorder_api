from collections import namedtuple

import arrow
from marshmallow import (Schema, ValidationError, fields, post_load, validates,
                         validates_schema)

from logger import logger

CreateOrder = namedtuple('CreateOrder', [
	'billing_unit_id',
	'rate',
	'power_limit',
	'energy_limit',
	'auto_renew',
	'renew_frequency',
	'start_date_time',
	'end_date_time',
	'user_group_id',
	'facility_id',
	'subnet_id',
	'prosumer_id'
])


class SellOrderSchema(Schema):
	billing_unit_id = fields.String(required=True)
	rate = fields.Integer(required=True)
	power_limit = fields.Float(allow_none=True)
	energy_limit = fields.Float(allow_none=True)
	auto_renew = fields.Boolean(required=True)
	renew_frequency = fields.String(allow_none=True)
	start_date_time = fields.String(allow_none=True)
	end_date_time = fields.String(allow_none=True)
	user_group_id = fields.String(required=True)
	facility_id = fields.String(required=True)
	subnet_id = fields.String(required=True)
	prosumer_id = fields.String(required=True)

	@post_load
	def create_order(self, data):
		return CreateOrder(**data)

	@validates_schema(pass_original=True)
	def validate_match(self, _, original_data):
		start_date_time = original_data['start_date_time']
		end_date_time = original_data['end_date_time']

		print("\n \n \n", bool(start_date_time))
		if start_date_time:
			start_date_time = arrow.get(start_date_time).replace(tzinfo="Africa/Lagos")
			current_date_time = arrow.now("Africa/Lagos")
			order_window = current_date_time.shift(minutes=5)

			if start_date_time < current_date_time:
				raise ValidationError('Start time cannot be in the past.')

			if start_date_time < order_window:
				raise ValidationError('Order made too soon. Please allow a window greater than 5 minutes.')

			if end_date_time:
				end_date_time = arrow.get(end_date_time).replace(tzinfo="Africa/Lagos")
				if start_date_time > end_date_time:
					raise ValidationError('End date must be greater than start date.')


	@validates('billing_unit_id')
	def validate_billing_unit(self, value):
		if not value:
			raise ValidationError('Billing method required.')

	@validates('rate')
	def validate_rate(self, value):
		try:
			int(value)
			if value <= 0:
				raise ValidationError('Invalid rate.')
		except Exception:
			raise ValidationError('Invalid input.')

	@validates('power_limit')
	def validate_power_limit(self, value):
		if value is not None:
			try:
				int(value)
				if value < 0:
					raise ValidationError('Invalid power limit.')
			except Exception:
				raise ValidationError('Invalid entry')

	@validates('energy_limit')
	def validate_energy_limit(self, value):
		if value is not None:
			try:
				int(value)
				if value < 0:
					raise ValidationError('Invalid energy limit.')
			except Exception:
				raise ValidationError('Invalid entry')

	@validates('renew_frequency')
	def validate_renew_frequency(self, value):
		if value is not None:
			if not value:
				raise ValidationError('Renew frequency required.')

	@validates('start_date_time')
	def validate_start_date_time(self, value):
		if value:
			try:
				arrow.get(value)
			except arrow.parser.ParserError as e:
				logger.exception(e)
				raise ValidationError('Order start date required.')

	@validates('end_date_time')
	def validate_end_date_time(self, value):
		if value:
			try:
				arrow.get(value)
			except arrow.parser.ParserError as e:
				logger.exception(e)
				raise ValidationError('Order end date required.')

	@validates('auto_renew')
	def validate_auto_renew(self, value):
		if not isinstance(value, bool):
			raise ValidationError('Auto renewal information required.')

	@validates('user_group_id')
	def validate_user_group_id(self, value):
		if not value:
			raise ValidationError('User group ID required.')

	@validates('facility_id')
	def validate_facility_id(self, value):
		if not value:
			raise ValidationError('Facility ID required.')

	@validates("subnet_id")
	def validate_subnet_id(self, value):
		if not value:
			raise ValidationError("Subnet ID required")

	@validates("prosumer_id")
	def validate_prosumer_id(self, value):
		if not value:
			raise ValidationError("Subnet ID required")
