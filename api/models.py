
from uuid import uuid4

import arrow
from flask import current_app
from sqlalchemy import and_, or_
from sqlalchemy_utils import ArrowType

from api import db
from api.kafka_mixin import KafkaMixin
from logger import logger


class Base(db.Model):
	__abstract__ = True
	id_ = db.Column(db.Integer, autoincrement=True, primary_key=True)
	date_created = db.Column(db.DateTime, default=db.func.current_timestamp())
	date_modified = db.Column(db.DateTime, default=db.func.current_timestamp(),
							  onupdate=db.func.current_timestamp())


class SellOrder(KafkaMixin, Base):
	id_ = db.Column(db.Integer, autoincrement=True,
				   unique=True, primary_key=True)
	billing_unit_id = db.Column(db.String(50), nullable=False)
	rate = db.Column(db.Float, nullable=False)
	power_limit = db.Column(db.Float, nullable=True)
	energy_limit = db.Column(db.Float, nullable=True)
	start_date_time = db.Column(ArrowType(timezone=True), nullable=True)
	end_date_time = db.Column(ArrowType(timezone=True), nullable=True)
	auto_renew = db.Column(db.Boolean, nullable=True)
	renew_frequency = db.Column(db.String(50), nullable=True)
	user_group_id = db.Column(db.String(50), nullable=False)
	created_by = db.Column(db.String(50), nullable=False)
	order_id = db.Column(db.String(50), nullable=False)
	facility_id = db.Column(db.String(50), nullable=False)
	subnet_id = db.Column(db.String(50), nullable=False)
	prosumer_id = db.Column(db.String(50), nullable=False)

	def __init__(self, billing_unit_id, rate, power_limit, energy_limit, start_date_time, end_date_time, auto_renew,
	renew_frequency, user_group_id, created_by, facility_id, subnet_id, prosumer_id):
		self.billing_unit_id = billing_unit_id
		self.rate = rate
		self.power_limit = power_limit
		self.energy_limit = energy_limit
		self.start_date_time = start_date_time
		self.end_date_time = end_date_time
		self.auto_renew = auto_renew
		self.renew_frequency = renew_frequency
		self.user_group_id = user_group_id
		self.created_by = created_by
		self.facility_id = facility_id
		self.subnet_id = subnet_id
		self.prosumer_id = prosumer_id
		self.order_id = str(uuid4())

	@staticmethod
	def create_order(
		billing_unit_id,
		rate,
		power_limit,
		energy_limit,
		start_date_time,
		end_date_time,
		auto_renew,
		renew_frequency,
		user_group_id,
		created_by,
		facility_id,
		subnet_id,
		prosumer_id,
		):
		"""Method that saves the created plan in the database"""
		success = False
		order = SellOrder(
			billing_unit_id=billing_unit_id,
			rate=int(rate),
			power_limit=float(power_limit),
			energy_limit=float(energy_limit),
			start_date_time=arrow.get(start_date_time).replace(tzinfo="Africa/Lagos"),
			end_date_time=arrow.get(end_date_time).replace(tzinfo="Africa/Lagos"),
			auto_renew=auto_renew,
			renew_frequency=renew_frequency,
			user_group_id=user_group_id,
			created_by=created_by,
			facility_id=facility_id,
			subnet_id=subnet_id,
			prosumer_id=prosumer_id,
		)
		db.session.add(order)
		try:
			db.session.commit()
			db.session.refresh(order)
		except Exception as e:
			logger.exception(e)
			success = False
			db.session.rollback()
			return e, success
		success = True
		return order.to_dict(), success

	@staticmethod
	def edit_order(order, data):
		"""
			Method to edit an existing sell order
		"""
		order.billing_unit_id = data['billing_unit_id']
		order.rate = int(data['rate'])
		order.power_limit = float(data['power_limit'])
		order.energy_limit = float(data['energy_limit'])
		order.start_date_time = arrow.get(data['start_date_time']).replace(tzinfo="Africa/Lagos")
		order.end_date_time = arrow.get(data['end_date_time']).replace(tzinfo="Africa/Lagos")
		order.auto_renew = data['auto_renew']
		order.renew_frequency = data['renew_frequency']
		order.prosumer_id = data['prosumer_id']

		db.session.add(order)
		db.session.commit()
		db.session.refresh(order)
		return order.to_dict()

	@staticmethod
	def find_order_match(energy_rate, start_date_time, end_date_time, facility_id):
		"""
			Method to obtain a sell order matching a buy order.
		"""
		matches = SellOrder.query.filter_by(facility_id=facility_id).filter(SellOrder.energy_rate <= energy_rate
		).filter(SellOrder.start_date_time <= start_date_time).filter(SellOrder.end_date_time >= end_date_time).all()
		return matches

	@staticmethod
	def delete_order(order):
		"""Delete one order."""
		db.session.delete(order)
		db.session.commit()
		return

	@staticmethod
	def delete_all_orders(order_obj):
		"""Delete multiple orders."""
		for order in order_obj.all():
			db.session.delete(order)
		db.session.commit()
		return

	@staticmethod
	def get_order(order_id=None, user_group_id=None, facility_id=None, subnet_id=None):
		if order_id:
			order = SellOrder.query.filter_by(order_id=order_id).first()
			return order

		if user_group_id:
			order = SellOrder.query.filter_by(user_group_id=user_group_id)
			return order

		if facility_id:
			order = SellOrder.query.filter_by(facility_id=facility_id)
			return order

		if subnet_id:
			order = SellOrder.query.filter_by(subnet_id=subnet_id)
			return order

	@staticmethod
	def get_user_order(user_group_id):
		order = SellOrder.get_order(user_group_id=user_group_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def get_facility_order(facility_id):
		order = SellOrder.get_order(facility_id=facility_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def get_subnet_order(subnet_id):
		order = SellOrder.get_order(subnet_id=subnet_id)
		if not order.first():
			return None
		return order

	@staticmethod
	def one_order(order_id):
		order = SellOrder.get_order(order_id)
		if not order:
			return order
		return order.to_dict()

	@staticmethod
	def all_facility_orders(facility_id):
		orders = SellOrder.query.filter_by(facility_id=facility_id)
		return orders

	@staticmethod
	def facility_orders(facility_id, page_no, size):
		orders = SellOrder.all_facility_orders(facility_id)
		orders = orders.order_by(SellOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def all_subnet_orders(subnet_id):
		return SellOrder.query.filter_by(subnet_id=subnet_id)

	@staticmethod
	def subnet_orders(subnet_id, page_no, size):
		orders = SellOrder.all_subnet_orders(subnet_id)
		orders = orders.order_by(SellOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def billing_subnet_orders(subnet_id, billing_unit_id, page_no, size):
		orders = SellOrder.all_subnet_orders(subnet_id)\
			.filter_by(billing_unit_id=billing_unit_id)\
			.order_by(SellOrder.date_modified.desc())\
			.paginate(page_no, size, False)
		return orders

	@staticmethod
	def all_my_orders(user_group_id):
		orders = SellOrder.query.filter_by(user_group_id=user_group_id)
		return orders

	@staticmethod
	def my_orders(user_group_id, page_no, size):
		orders = SellOrder.all_my_orders(user_group_id)
		orders = orders.order_by(SellOrder.date_modified.desc()).paginate(page_no, size, False)
		return orders

	@staticmethod
	def clashing_times(user_group_id, subnet_id, facility_id, start_date_time, end_date_time):
		"""
			This method checks for time clashes between orders.
			It prevents orders from being created that overright the run time of an existing order.
			This strictly checks the time fields of orders.
			If a change is to be made, an EDIT to the order is advised.
			This query makes use of several conditions to check an order.
			It is not immediately obvious how to tell the user what condition they are violating.
			The inspiration for this query is found in the stackoverflow answer here: https://stackoverflow.com/a/46161522/4909255
		"""
		start_date_time_between_existing_order = and_(SellOrder.start_date_time <= start_date_time, SellOrder.end_date_time > start_date_time).self_group()
		end_date_time_between_existing_order = and_(SellOrder.start_date_time < end_date_time, SellOrder.end_date_time >= end_date_time).self_group()
		order_within_existing_order = and_(SellOrder.start_date_time <= start_date_time, SellOrder.end_date_time >= end_date_time).self_group()
		order_enveloping_existing_order = and_(SellOrder.start_date_time > start_date_time, SellOrder.end_date_time < end_date_time).self_group()

		clash_conditions = [start_date_time_between_existing_order, end_date_time_between_existing_order, order_within_existing_order, order_enveloping_existing_order]

		# Clash occurs when one of the clash conditions is met
		or_clauses = or_(*clash_conditions)
		time_clash = SellOrder.query.filter_by(user_group_id=user_group_id).filter_by(subnet_id=subnet_id).filter_by(facility_id=facility_id).filter(or_clauses).all()

		return time_clash

	def payload_format(self):
		dict_order = {
			'billing_unit_id': self.billing_unit_id,
			'rate': self.rate,
			'power_limit': self.power_limit,
			'energy_limit': self.energy_limit,
			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),
			'auto_renew': self.auto_renew,
			'renew_frequency': self.renew_frequency,
			'user_group_id': self.user_group_id,
			'facility_id': self.facility_id,
			'subnet_id': self.subnet_id,
		}
		return dict_order

	def edit_format(self):
		dict_order = {
			'billing_unit_id': self.billing_unit_id,
			'rate': self.rate,
			'power_limit': self.power_limit,
			'energy_limit': self.energy_limit,
			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),
			'auto_renew': self.auto_renew,
			'renew_frequency': self.renew_frequency
		}
		return dict_order

	def kafka_dict(self):
		dict_order = {
			'kafka_topic': current_app.config.get('NEW_SELLORDER_TOPIC'),

			'user_group_id': self.user_group_id,
			'prosumer_id': self.prosumer_id,
			'order_id': self.order_id,

			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),

			'rate': self.rate,

			'power_limit': self.power_limit,
			'energy_limit': self.energy_limit,

			'auto_renew': self.auto_renew,
			'renew_frequency': self.renew_frequency,

			'billing_unit_id': self.billing_unit_id,
		}
		return dict_order

	def to_dict(self):
		date_created = str(arrow.get(self.date_created))
		dict_order = {
			'billing_unit_id': self.billing_unit_id,
			'rate': self.rate,
			'power_limit': self.power_limit,
			'energy_limit': self.energy_limit,
			'start_date_time': str(self.start_date_time),
			'end_date_time': str(self.end_date_time),
			'auto_renew': self.auto_renew,
			'renew_frequency': self.renew_frequency,
			'facility_id': self.facility_id,
			'subnet_id': self.subnet_id,
			'user_group_id': self.user_group_id,
			'order_id': self.order_id,
			'created_by': self.created_by,
			'date_created': date_created,
		}
		return dict_order


db.event.listen(db.session, 'before_commit', KafkaMixin.before_commit)
db.event.listen(db.session, 'after_commit', KafkaMixin.after_commit)
