from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from config import config
from flask_restplus import Api


authorizations = {
	'apikey': {
		'type': 'apiKey',
		'in': 'header',
		'name': 'X-API-KEY'
	}
}
db = SQLAlchemy()
api = Api(doc='/doc/', authorizations=authorizations)


def create_api(config_name):
	app = Flask(__name__)
	try:
		init_config = config[config_name]()
	except KeyError:
		raise
	except Exception:
		# For unforseen exceptions
		raise
		exit()

	print('Running in {} Mode'.format(init_config))
	config_object = config.get(config_name)

	app.config.from_object(config_object)
	config_object.init_app(app)

	db.init_app(app)

	from api.controllers import sell_api as ns1
	api.add_namespace(ns1, path='/sell')

	api.init_app(app, authorizations=authorizations)

	return app
