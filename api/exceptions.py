"""Custom api exceptions."""


class KafkaConnectionError(Exception):
	"""Kafka broker connection cannot be established."""
	pass


class TopicMissingError(Exception):
	"""Kafka topic not found."""
	pass


class ValueSchemaNotFound(Exception):
	"""Avro schema not found."""
	pass
