import os
from ast import literal_eval


class Config(object):
	DEBUG = False
	SQLALCHEMY_TRACK_MODIFICATIONS = False
	JWT_KEY = os.environ['JWT_KEY']
	SECRET_KEY = os.environ['SECRET_KEY']
	MIGRATE_DB = os.environ['MIGRATE_DB']

	BOOTSTRAP_SERVERS = os.environ['BOOTSTRAP_SERVERS']
	SCHEMA_REGISTRY_URI = os.environ['SCHEMA_REGISTRY_URI']
	NEW_SELLORDER_TOPIC = os.environ['NEW_SELLORDER_TOPIC']
	TOPICS = literal_eval(os.environ['TOPICS'])
	SCHEMA_MAPPING = literal_eval(os.environ['SCHEMA_MAPPING'])
	TOPIC_SCHEMA_MAPPING = dict(zip(TOPICS, SCHEMA_MAPPING))



class Development(Config):
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', 'sqlite://')

	@classmethod
	def init_app(cls, app):
		return app

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Testing(Config):
	TESTING = True
	SQLALCHEMY_DATABASE_URI = 'sqlite://'


class Staging(Config):
	"""TODO"""
	DEBUG = True
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

	@classmethod
	def init_app(cls, app):
		from api.producer import KafkaProducer
		app.kafka_producer = KafkaProducer(
			bootstrap_servers=cls.BOOTSTRAP_SERVERS,
			schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
			topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
		)
		app.kafka_producer.init_schema()

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


class Documentation(Config):

	@classmethod
	def init_app(cls, app):
		pass


class Production(Config):
	SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')

	@classmethod
	def init_app(cls, app):
		from api.producer import KafkaProducer
		app.kafka_producer = KafkaProducer(
			bootstrap_servers=cls.BOOTSTRAP_SERVERS,
			schema_registry_uri=cls.SCHEMA_REGISTRY_URI,
			topic_schema_mapping=cls.TOPIC_SCHEMA_MAPPING,
		)
		app.kafka_producer.init_schema()

	def __repr__(self):
		return '{}'.format(self.__class__.__name__)


config = {
	'development': Development,
	'testing': Testing,
	'Production': Production,
	'Staging': Staging,
	'doc': Documentation,

	'default': Development,
}
